//GLOBAL LISTENER
function handleAuthUrl(data,redirect){
  //console.log(data);
}
FlowRouter.triggers.enter([handleAuthUrl])
FlowRouter.route('/', {

  name: 'login',

  action: function () {

    BlazeLayout.setRoot('body');
    BlazeLayout.render("commonLayout", {main: "login"});
  }

});

FlowRouter.route('/changePword/:token', {

  name: 'pWord',

  action: function () {

    BlazeLayout.setRoot('body');
    BlazeLayout.render("commonLayout", {main: "pWord"});
  }

});
FlowRouter.route('/register', {

  name: 'register',

  action: function () {

    BlazeLayout.setRoot('body');
    BlazeLayout.render("commonLayout", {main: "register"});
  }

});

FlowRouter.route('/account', {
  name: 'account',
  action: function () {
    BlazeLayout.setRoot('body');
    BlazeLayout.render("commonLayout", {main: "account"});
  }
});
