var bcrypt = require('bcrypt');
var SHA256= require('sha256');
Meteor.methods({
  emailResetPassword: function(emailVar){
    check(emailVar, String);
    var res = Accounts.findUserByEmail(emailVar);
    if(res){
      Accounts.sendResetPasswordEmail(res._id);
      return true;
    }else{
      throw new Meteor.Error(403,"Email not found");
      return ;
    }
  },
  'upload_avatar':function(id,avatar){
    check(id,String);
    check(avatar,String);
    //console.log(avatar);
    var res=Meteor.users.update({_id:id},{$set:{avatar:avatar}});
    console.log(res);
    return res;
  },
'login_user':function(emailVar,passwordVar){
  check(emailVar,String);
  check(passwordVar,String);
  //console.log(emailVar);
  //console.log(Meteor.users.findOne());
  var user=Meteor.users.findOne({"emails.address":emailVar});
  if(user){
    if(user.services.password.algorithm=='sha-256'){
      if(user.services.password.digest==SHA256(passwordVar)){
        console.log(user.services.password.digest,SHA256(passwordVar));
        return true;
      }else{
        console.log(user);
        return false;
      }
    }else{
        return false;
    }
  }else{
    return false;
  }
}
});
