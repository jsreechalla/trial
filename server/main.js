import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {

        Accounts.onCreateUser(function(options,user){
          if(user.services.google){
            user.profile={};
            //user.emails[0].address="";
            //user.emails[0].address=user.services.google.email;
            user.avatar=user.services.google.picture;
            user.profile.name=user.services.google.name;
          }else{
            console.log(options,user);
            user.profile={};
              user.emails[0].address=options.email;
              user.services.password=options.password;
              user.profile.name=options.name;

          }
          return user;
        });
});
