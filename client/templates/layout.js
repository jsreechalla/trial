Meteor.settings = {
  "public":{
    "walletPricing":{
      "perTrade": 100,
      "portfolio": 1500
    }
  }
}

Template.layout.onCreated(() => {
  // to disable back button
  // window.location.hash="no-back-button";
  // window.location.hash="Again-No-back-button";
  // window.onhashchange=function(){window.location.hash="no-back-button";}

  // for idle time

  let idleTime = 0;
  $(document).ready(function () {
    //Increment the idle time counter every minute.
    let idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
      idleTime = 0;
    });
    $(this).keypress(function (e) {
      idleTime = 0;
    });
  });

  function timerIncrement() {
    console.log("idle time tracker");
    idleTime = idleTime + 1;
    if (idleTime > 59) { // 60 minutes
      Meteor.logout(() =>{
        FlowRouter.go("/");
      });
    }
  }
})

Template.layout.helpers({
  isNotAdmin:()=>{

    var route = FlowRouter.current();
    if (route.route.name === 'verifyEmail' || route.route.name === 'resetPassword'){
      return true;
    }

    var __user = Meteor.user();
    if (__user && Meteor.userId()){
      if (__user && __user.emails && __user.emails[0].verified && !Roles.userIsInRole(__user._id, 'admin', 'default-group'))
      {
        return true;
      }
      else{
        Meteor.logout(()=>{
          window.location.href = window.location.origin;
        });
        return false;
      }
    }
    else{
      if (!Meteor.userId()){
        window.location.href = window.location.origin;
        return true;
      }
    }
  },
  checkSubscription:()=>{
    // if (Meteor.userId()){
    //   if (Meteor.user()){
    //     var user = Meteor.user();
    //     if (user.emails[0].verified){
    //       if (user.subscription || user.wallet){
    //         if (user.subscription && user.wallet){
    //           if (user.subscription.exp_date < new Date() && user.wallet.balance === 0){//if subscription expired
    //             return false;
    //           }
    //         }
    //         else if (user.subscription){//if already subscribed
    //           if (user.subscription.exp_date < new Date()){//if subscription expired
    //             return false;
    //           }
    //         }
    //       }
    //       else{
    //         return false;
    //       }
    //     }
    //   }
    // }
    return true;
  }
})

Template.layout.events({
  'keypress .numberOnly': function(evt, t){
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  },
});
