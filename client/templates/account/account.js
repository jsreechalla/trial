Template.account.helpers({
email:function(){
  var user = Meteor.users.findOne({_id:Meteor.user()._id});
  if(user){
  if(!user.emails){
    return user.profile.name+"(loginWithGoogle)";
  }else{
      return user.emails[0].address;
    }
  }
  else{

    return '';
  }
},
phone:function(){
  var user = Meteor.users.findOne({_id:Meteor.userId()});
  if(user){
  if(user.phoneNo){
    return user.phoneNo;
  }else{
    return "";
  }
}else{
  FlowRouter.go("/");
}
},
avatar:function(){
  var user = Meteor.users.findOne({_id:Meteor.userId()});
  console.log(user.avatar);
  if(user){
    if(user.avatar){
      return user.avatar;
    }else{
      return '';
    }
  }else{
    FlowRouter.go("/");
  }

},
name:function(){
  var user = Meteor.users.findOne({_id:Meteor.userId()});
  if(user.profile.name){
    return user.profile.name;
  }else{
    return "";
  }
},
});
Template.account.events({
  'click #signout': () => {
    Meteor.logout(() => {
      FlowRouter.go("/");
    })
  },
"submit #user":function(e,t){
  e.preventDefault();
  var pold=document.getElementById("p1").value;
  var pword1=document.getElementById("pword1").value;
  var pword2=document.getElementById("pword2").value;
    if(!pold){
      toastr.error("Enter all the fields","Error");
    }
    if(!pword1||!pword2){
      toastr.error("Enter all the fields","Error");
    }
    if(pold===pword1){
      toastr.error("New Password shouldn't match the new one","Error");
    }
    if(pword1!==pword2){
      toastr.error("Passwords don't match","Error");
    }
    else if(pword1===pword2) {
      Accounts.changePassword(pold, pword1, function (error) {
        if(error){
          toastr.error(error.reason, "Error");
        }else{
          toastr.success("Password changed. Please login again.", "Success");
          Meteor.logout(function(){
            FlowRouter.go("/");
          })
        }
      });
    }
    else{
      toastr.error(error.message,"Error");
    }
},
"click #details":function(e,t){
  e.preventDefault();
  var name=document.getElementById("name").value;
  var phone=document.getElementById("phone").value;
  if(!name){
    toastr.error("Enter all the fields","Error");
  }
  else if(!phone){
    toastr.error("Enter all the fields","Error");
  }
  else{
    console.log(Meteor.userId());
    Meteor.users.update({_id:Meteor.userId()},{$set:{"profile.name":name,"profile.phoneNo":phone}});
    toastr.success("Profile Details updated","Success");
  }
},
'change .upload':function(e,t){
      // var id=e.target.id;
      var preview=document.querySelector("img");
      var file= e.target.files[0];
      var reader =new FileReader();
      console.log(file,e.target);
        reader.onload =function(event){
        preview.src=reader.result;
        t.dataDict.set("url",reader.result);
        }
      reader.readAsDataURL(file)
       console.log(file);
    },
    'click #dp':function(e,t){
      e.preventDefault();
      var id=Meteor.user()._id;
      var avatar=t.dataDict.get("url");
      console.log(avatar);
      Meteor.call('upload_avatar',id,avatar,function(err,res){
        if(err){
          toastr.error(err.message,"Error");
        }else{
          toastr.success("Avatar uploaded","Success")
        }
      });
    },
});

Template.account.onCreated(function onCreated(){
this.dataDict = new ReactiveDict();
});
Template.account.onRendered(function(){
  if (!validUser()) {
    toastr.error("Login to see this Page", "Error");
    FlowRouter.go("/");
  }
});
