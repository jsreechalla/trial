/**
 * Created by rohith on 14/10/2016 AD.
 */
Template.login.onCreated(function(){
  /*if(validateAdmin){
    FlowRouter.go("/dashboard");
  }*/
});
Template.login.events({
    'click #login': function(event, t){
        event.preventDefault();
        var emailVar = t.$("#loginEmail").val();
        var passwordVar = t.$("#loginPassword").val();
        var res= validEmail(emailVar);
        if( !emailVar || !passwordVar ){
            toastr.error("Email or Password cannot be empty.", "Error");
            return;
          }if(!res){
            toastr.error("Enter a valid email","Error");
          }
          else{
            Meteor.call("login_user",emailVar,passwordVar,function(err,res){
              if(err){
                console.log("fjhakfsd",err);
              }else{
                console.log(res);
                  FlowRouter.go("/account");
              }
            });
          }
    },
    'click #changePword':function(event,t){
      event.preventDefault();
      var emailVar = t.$("#loginEmail").val();
      // let user= Meteor.user();
      if(!emailVar){
        toastr.error("Email cannot be empty.", "Error");
      }else{
        Meteor.call('emailResetPassword', emailVar, function (error, result) {
        if(error){
          toastr.error(error.reason, "Error");
        }else{
          toastr.success("Email Sent.", "Success");
          FlowRouter.go("/");
        }
      });
      }
    },
    'click #google':function(event,t){
      event.preventDefault();
      console.log("google");
      Meteor.loginWithGoogle(
        function(err,res){
        if(err){
          console.log(err);
        }else{
          FlowRouter.go("/account");
        }
      });
    },
    'click #register':function(){
      FlowRouter.go('/register');
    }
});
Template.login.onRendered(function onRendered(){

});
