Template.pWord.onRendered(function(){

});

Template.pWord.events({
  'submit form': function(e){
    e.preventDefault();
    let newPassword = $("#pword1").val();
    let newPassword1 = $("#pword2").val();
    let token = FlowRouter.getParam("token");
    if( !newPassword || !newPassword1){
      toastr.error("All fields are required", "Error");
      return;
    }
    if( newPassword !== newPassword1){
      toastr.error("The new password you entered twice, does not match", "Error");
      return;
    }
    Accounts.resetPassword(token, newPassword, function (error) {
      if(error){
        toastr.error(error.reason, "Error");
      }else{
        toastr.success("Password changed. Please login again.", "Success");

        Meteor.logout(function(){
          FlowRouter.go("/");
        })
      }
    });
  }
})
